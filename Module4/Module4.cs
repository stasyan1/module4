﻿using System;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
           //PROJECT
        }


        public int Task_1_A(int[] array)
        {
            int max=array[0];
            for (int i = 0; i < array.Length; i++)
            {
              if (array[i] > max)
              max = array[i];
            }
            return max;
        }

        public int Task_1_B(int[] array)
        {
            int min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
              if (array[i]<0)
              min= array[i];
            }
            return min;
        }

        public int Task_1_C(int[] array)
        {
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
             sum += array[i];
            return sum;
        }

        public int Task_1_D(int[] array)
        {
            

            int max = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > max)
                    max = array[i];
            }
           
            int min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                    min = array[i];
            }
            int raznost = max - min;
            return raznost;

        }

        public void Task_1_E(int[] array)
        {

            int max = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > max)
                    max = array[i];
            }
            int min = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < 0)
                    min = array[i];
            }

            for (int i=0;i<array.Length;i++)
            {
                if (array[i] % 2 != 0)
                array[i]=array[i] - min;   
            }
            
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] % 1 != 0)
                array[i] = array[i] + max; 
            }
            
        }

        public int Task_2(int a, int b, int c)
        {
            int sum = a + b + c;
            return sum;
        }

        public int Task_2(int a, int b)
        {
            int sum = a + b;
            return sum;
        }

        public double Task_2(double a, double b, double c)
        {

            double sum = a + b + c;
            return sum;
        }

        public string Task_2(string a, string b)
        {
            string str = a + " " +b;
            return str;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            int[] c = new int[b.Length];
            for (int i = 0; i < b.Length; i++)
            {
                if (i < a.Length)
                     c[i] = a[i] + b[i];
                else
                    c[i] = b[i];
            }
            return c;

        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;

        }

        public void Task_3_B(double radius, out double length, out double square)
        {

            if (radius < 0) throw new ArgumentNullException(nameof(radius));
            length = 2 * Math.PI * radius;
            square = Math.PI * Math.Pow(radius, 2);

        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            try
            {
                if (array.Length == 0) throw new ArgumentNullException(nameof(array));
                maxItem = Task_1_A(array);
                minItem = Task_1_B(array);
                sumOfItems = Task_1_C(array);
            }
            catch
            {
                throw new ArgumentNullException(nameof(array));
            }

        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;
            return numbers;

        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0) throw new ArgumentNullException(nameof(radius));
            (double, double) circle;
            circle.Item1 = 2 * Math.PI * radius;
            circle.Item2 = Math.PI * Math.Pow(radius, 2);
            return circle;

        }

        public (int, int, int) Task_4_C(int[] array)
        {
            try
            {
                if (array.Length == 0) throw new ArgumentNullException(nameof(array));
                (int, int, int) arrayInfo;
                arrayInfo.Item1 = Task_1_B(array);
                arrayInfo.Item2 = Task_1_A(array);
                arrayInfo.Item3 = Task_1_C(array);
                return arrayInfo;
            }
            catch
            {
                throw new ArgumentNullException(nameof(array));
            }

        }

        public void Task_5(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            { array[i] = array[i] + 5;
                
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            int temp;
            if (direction == SortDirection.Ascending)
            { for (int i = 0; i < array.Length - 1; i++)
                {
                    for (int j = i + 1; j < array.Length; j++)
                    {
                        if (array[i] > array[j])
                        {
                            temp = array[i];
                            array[i] = array[j];
                            array[j] = temp;
                        }
                    }
                }
            }
            else {
                for (int i = 0; i < array.Length - 1; i++)
                {
                    for (int j = i + 1; j < array.Length; j++)
                    {
                        if (array[i] < array[j])
                        {
                            temp = array[i];
                            array[i] = array[j];
                            array[j] = temp;
                        }
                    }
                }
            }

        }        

        public  double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            try
            {
                double c = (x1 + x2) / 2;
                if (func(c) == 0 || x1 + x2 < e) return c;
                if (func(x1) * func(c) < 0) return Task_7(func, x1, c, e, result);
                return Task_7(func, c, x2, e, result);
            }
            catch
            {
                throw new ArgumentNullException(nameof(func));
            }

        }
    }
}
